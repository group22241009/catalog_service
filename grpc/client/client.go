package client

import (
	"catalog_service/genproto/catalog_service"

	"catalog_service/config"

	"google.golang.org/grpc"
)

type IServiceManager interface {
	ProductService() catalog_service.ProductServiceClient
	CategoryService() catalog_service.CategoryServiceClient
}

type grpcClients struct {
	productService  catalog_service.ProductServiceClient
	categoryService catalog_service.CategoryServiceClient
}

func NewGrpcClients(cfg config.Config) (IServiceManager, error) {
	connCatalogService, err := grpc.Dial(
		cfg.ServiceGrpcHost+cfg.ServiceGrpcPort,
		grpc.WithInsecure(),
	)
	if err != nil {
		return nil, err
	}

	return &grpcClients{
		productService:  catalog_service.NewProductServiceClient(connCatalogService),
		categoryService: catalog_service.NewCategoryServiceClient(connCatalogService),
	}, nil
}

func (g *grpcClients) ProductService() catalog_service.ProductServiceClient {
	return g.productService
}
func (g *grpcClients) CategoryService() catalog_service.CategoryServiceClient {
	return g.categoryService
}
