package storage

import (
	pbo "catalog_service/genproto/catalog_service"
	"context"

	"google.golang.org/protobuf/types/known/emptypb"
)

type IStorage interface {
	Close()
	Category() ICategoryStorage
	Product() IProductStorage
}
type ICategoryStorage interface {
	Create(context.Context, *pbo.CreateCategory) (*pbo.Category, error)
	Get(context.Context, *pbo.CategoryPrimaryKey) (*pbo.Category, error)
	GetList(context.Context, *pbo.GetListRequestCategory) (*pbo.CategoryListResponse, error)
	Update(context.Context, *pbo.Category) (*pbo.Category, error)
	Delete(context.Context, *pbo.CategoryPrimaryKey) (*emptypb.Empty, error)
}
type IProductStorage interface {
	Create(context.Context, *pbo.CreateProduct) (*pbo.Product, error)
	Get(context.Context, *pbo.PrimaryKeyProduct) (*pbo.Product, error)
	GetList(context.Context, *pbo.GetListRequestProduct) (*pbo.ProductListResponse, error)
	Update(context.Context, *pbo.Product) (*pbo.Product, error)
	Delete(context.Context, *pbo.PrimaryKeyProduct) (*emptypb.Empty, error)
}
