package postgres

import (
	pbo "catalog_service/genproto/catalog_service"
	"catalog_service/pkg/helper"
	"catalog_service/pkg/logger"
	"catalog_service/storage"
	"context"
	"fmt"

	"github.com/google/uuid"
	"github.com/jackc/pgx/v5/pgxpool"
	"google.golang.org/protobuf/types/known/emptypb"
)

type categoryRepo struct {
	db  *pgxpool.Pool
	log logger.ILogger
}

func NewCatgoryRepo(db *pgxpool.Pool, log logger.ILogger) storage.ICategoryStorage {
	return &categoryRepo{
		db:  db,
		log: log,
	}
}
func (d *categoryRepo) Create(ctx context.Context, request *pbo.CreateCategory) (*pbo.Category, error) {
	var (
		category = pbo.Category{}
		err      error
	)
	query := `insert into categories(id,title,active,parent_id,order_number,created_at) 
	          values($1,$2,$3,$4,$5,now())
			  returning id,title,active,parent_id,order_number`

	// searchingColumn := fmt.Sprintf("%s %d ", request.GetTitle(), request.GetOrderNumber())
	categoryID := uuid.New().String()
	request.ParentId = categoryID
	if err = d.db.QueryRow(ctx, query, categoryID, request.GetTitle(), request.GetActive(), request.GetParentId(), request.GetOrderNumber()).Scan(
		&category.Id,
		&category.Title,
		&category.Active,
		&category.ParentId,
		&category.OrderNumber,
	); err != nil {
		d.log.Error("err", logger.Error(err))
		return nil, err
	}

	return &category, nil

}
func (d *categoryRepo) Get(ctx context.Context, request *pbo.CategoryPrimaryKey) (*pbo.Category, error) {
	category := pbo.Category{}
	query := ` select id, title, active, parent_id, order_number from categories where deleted_at=0 and id=$1`
	if err := d.db.QueryRow(ctx, query, request.GetId()).Scan(
		&category.Id,
		&category.Title,
		&category.Active,
		&category.ParentId,
		&category.OrderNumber,
	); err != nil {
		d.log.Error("err", logger.Error(err))
		return nil, err
	}
	return &category, nil

}
func (d *categoryRepo) GetList(ctx context.Context, request *pbo.GetListRequestCategory) (*pbo.CategoryListResponse, error) {
	var (
		resp   = pbo.CategoryListResponse{}
		filter = " where deleted_at=0"
		offset = (request.GetPage() - 1) * request.GetLimit()
		count  = int32(0)
	)
	if request.Search != "" {
		filter += fmt.Sprintf(" and searching_column ilike '%s'", request.GetSearch())
	}
	countQuery := `select count(*) from categories ` + filter
	if err := d.db.QueryRow(ctx, countQuery).Scan(&count); err != nil {
		d.log.Error("error while scanning count ", logger.Error(err))
		return nil, err
	}

	query := `select id,title,active,parent_id,order_number from categories ` + filter + fmt.Sprintf("offset %d limit %d", offset, request.GetLimit())
	rows, err := d.db.Query(ctx, query)
	if err != nil {
		d.log.Error("error while getting", logger.Error(err))
		return nil, err
	}
	defer rows.Close()

	for rows.Next() {
		category := pbo.Category{}

		if err = rows.Scan(
			&category.Id,
			&category.Title,
			&category.Active,
			&category.ParentId,
			&category.OrderNumber,
		); err != nil {
			d.log.Error("error getting list", logger.Error(err))
			return nil, err
		}
		resp.Categories = append(resp.Categories, &category)
	}

	resp.Count = count
	return &resp, nil

}
func (d *categoryRepo) Update(ctx context.Context, request *pbo.Category) (*pbo.Category, error) {
	var (
		category = pbo.Category{}
		params   = make(map[string]interface{})
		query    = ` update categories set `
		filter   = ""
	)

	params["id"] = request.GetId()
	fmt.Println("category_id", request.GetId())

	if request.GetTitle() != "" {
		params["title"] = request.GetTitle()
		filter += " title=@title "
	}
	if request.GetOrderNumber() != 0 {
		params["order_number"] = request.GetOrderNumber()
		filter += " order_number=@order_number "
	}
	if request.GetActive() {
		params["active"] = request.GetActive()
		filter += " active = @active "
	}

	query += filter + ` updated_at = now() where deleted_at = 0 and id = $1 returning id, title, active, parent_id, order_number `

	fullQuery, args := helper.ReplaceQueryParams(query, params)
	if err := d.db.QueryRow(ctx, fullQuery, args...).Scan(
		&category.Id,
		&category.Title,
		&category.Active,
		&category.ParentId,
		&category.OrderNumber,
	); err != nil {
		d.log.Error("err", logger.Error(err))
		return nil, err
	}

	return &category, nil

}
func (d *categoryRepo) Delete(ctx context.Context, request *pbo.CategoryPrimaryKey) (*emptypb.Empty, error) {
	query := `update categories set deleted_at = extract(epoch from current_timestamp) where id = $1`
	_, err := d.db.Exec(ctx, query, request.Id)
	return nil, err
}
