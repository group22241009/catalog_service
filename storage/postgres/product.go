package postgres

import (
	pbo "catalog_service/genproto/catalog_service"
	"catalog_service/pkg/helper"
	"catalog_service/pkg/logger"
	"catalog_service/storage"
	"context"
	"fmt"

	"github.com/google/uuid"
	"github.com/jackc/pgx/v5/pgxpool"
	"google.golang.org/protobuf/types/known/emptypb"
)

type productRepo struct {
	db  *pgxpool.Pool
	log logger.ILogger
}

func NewProductRepo(db *pgxpool.Pool, log logger.ILogger) storage.IProductStorage {
	return &productRepo{
		db:  db,
		log: log,
	}
}
func (d *productRepo) Create(ctx context.Context, request *pbo.CreateProduct) (*pbo.Product, error) {
	var (
		product = pbo.Product{}
		err     error
	)
	query := ` insert into products(id,title,description,order_number,active,type,price,category_id,searching_column) 
	          values($1,$2,$3,$4,$5,$6,$7)
			  returning id,title,description,order_number,active,type,price `

	searchingColumn := fmt.Sprintf("%s %s %d %f", request.GetTitle(), request.GetDescription(), request.GetOrderNumber(), request.GetPrice())
	if err = d.db.QueryRow(ctx, query, uuid.New().String(), request.GetTitle(), request.GetDescription(), request.GetOrderNumber(), request.GetActive(), "product", request.GetPrice(), request.GetCategoryId(), searchingColumn).Scan(
		&product.Id,
		&product.Title,
		&product.Description,
		&product.OrderNumber,
		&product.Active,
		"product",
		&product.Price,
		&product.CategoryId,
	); err != nil {
		d.log.Error("err", logger.Error(err))
		return nil, err
	}

	return &product, nil

}
func (d *productRepo) Get(ctx context.Context, request *pbo.PrimaryKeyProduct) (*pbo.Product, error) {
	product := pbo.Product{}
	query := ` select id,title,description,order_number,active,type,price,category_id from products where deleted_at=0 and id=$1`
	if err := d.db.QueryRow(ctx, query, request.GetId()).Scan(
		&product.Id,
		&product.Title,
		&product.Description,
		&product.OrderNumber,
		"product",
		&product.Price,
		&product.CategoryId,
	); err != nil {
		d.log.Error("err", logger.Error(err))
		return nil, err
	}
	return &product, nil

}
func (d *productRepo) GetList(ctx context.Context, request *pbo.GetListRequestProduct) (*pbo.ProductListResponse, error) {
	var (
		resp   = pbo.ProductListResponse{}
		filter = " where deleted_at=0"
		offset = (request.GetPage() - 1) * request.GetLimit()
		count  = int32(0)
	)
	if request.Search != "" {
		filter += fmt.Sprintf(" and searching_column ilike '%s'", request.GetSearch())
	}
	countQuery := `select count(*) from products ` + filter
	if err := d.db.QueryRow(ctx, countQuery).Scan(&count); err != nil {
		d.log.Error("error while scanning count ", logger.Error(err))
		return nil, err
	}

	query := `  select id,title,description,order_number,active,type,price,category_id from products` + filter + fmt.Sprintf("offset %d limit %d", offset, request.GetLimit())
	rows, err := d.db.Query(ctx, query)
	if err != nil {
		d.log.Error("error while getting", logger.Error(err))
		return nil, err
	}
	defer rows.Close()

	for rows.Next() {
		product := pbo.Product{}

		if err = rows.Scan(
			&product.Id,
			&product.Title,
			&product.Description,
			&product.OrderNumber,
			&product.Active,
			"product",
			&product.Price,
			&product.CategoryId,
		); err != nil {
			d.log.Error("error getting list", logger.Error(err))
			return nil, err
		}
		resp.Products = append(resp.Products, &product)
	}

	resp.Count = count
	return &resp, nil

}
func (d *productRepo) Update(ctx context.Context, request *pbo.Product) (*pbo.Product, error) {
	var (
		product = pbo.Product{}
		params  = make(map[string]interface{})
		query   = `update products set `
		filter  = ""
	)

	params["id"] = request.GetId()
	fmt.Println("product_id", request.GetId())

	if request.GetTitle() != "" {
		params["title"] = request.GetTitle()
		filter += " title=@title"
	}
	if request.GetDescription() != "" {
		params["description"] = request.GetDescription()
		filter += " description=@description"
	}
	if request.GetCategoryId() != "" {
		params["category_id"] = request.GetCategoryId()
		filter += "category_id=@category_id "
	}
	if request.GetPrice() != 0.0 {
		params["price"] = request.GetPrice()
		filter += "price=@price"
	}
	if request.GetOrderNumber() != 0 {
		params["order_number"] = request.GetOrderNumber()
		filter += "order_number=@order_number"
	}
	if request.GetActive() {
		params["active"] = request.GetActive()
		filter += " active = @active"
	}

	query += filter + ` updated_at = now() where deleted_at = 0 and id = @id returning id,title,description,order_number,active,type,price,category_id `

	fullQuery, args := helper.ReplaceQueryParams(query, params)
	if err := d.db.QueryRow(ctx, fullQuery, args...).Scan(
		&product.Id,
		&product.Title,
		&product.Description,
		&product.OrderNumber,
		&product.Active,
		"product",
		&product.Price,
		&product.CategoryId,
	); err != nil {
		d.log.Error("err", logger.Error(err))
		return nil, err
	}

	return &product, nil

}
func (d *productRepo) Delete(ctx context.Context, request *pbo.PrimaryKeyProduct) (*emptypb.Empty, error) {

	query := `update products set deleted_at = extract(epoch from current_timestamp) where id = $1`
	_, err := d.db.Exec(ctx, query, request.Id)
	return nil, err
}
