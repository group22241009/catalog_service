package service

import (
	"context"
	"catalog_service/grpc/client"
	"catalog_service/pkg/logger"
	"catalog_service/storage"

	pbo "catalog_service/genproto/catalog_service"

	"google.golang.org/protobuf/types/known/emptypb"
)

type productService struct {
	storage  storage.IStorage
	services client.IServiceManager
	log      logger.ILogger
}

func NewProductService(strg storage.IStorage, services client.IServiceManager, log logger.ILogger) *productService {
	return &productService{
		storage:  strg,
		services: services,
		log:      log,
	}
}
func (o *productService) Create(ctx context.Context, request *pbo.CreateProduct) (*pbo.Product, error) {
	return o.storage.Product().Create(ctx, request)
}
func (o *productService) Get(ctx context.Context, request *pbo.PrimaryKeyProduct) (*pbo.Product, error) {
	return o.storage.Product().Get(ctx, request)
}
func (o *productService) GetList(ctx context.Context, request *pbo.GetListRequestProduct) (*pbo.ProductListResponse, error) {
	return o.storage.Product().GetList(ctx, request)
}
func (o *productService) Update(ctx context.Context, request *pbo.Product) (*pbo.Product, error) {
	return o.storage.Product().Update(ctx, request)
}
func (o *productService) Delete(ctx context.Context, request *pbo.PrimaryKeyProduct) (*emptypb.Empty, error) {
	return o.storage.Product().Delete(ctx, request)
}
