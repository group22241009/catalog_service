package service

import (
	"context"
	"catalog_service/grpc/client"
	"catalog_service/pkg/logger"
	"catalog_service/storage"

	pbo "catalog_service/genproto/catalog_service"

	"google.golang.org/protobuf/types/known/emptypb"
)

type categoryService struct {
	storage  storage.IStorage
	services client.IServiceManager
	log      logger.ILogger
}

func NewCategoryService(strg storage.IStorage, services client.IServiceManager, log logger.ILogger) *categoryService {
	return &categoryService{
		storage:  strg,
		services: services,
		log:      log,
	}
}
func (o *categoryService) Create(ctx context.Context, request *pbo.CreateCategory) (*pbo.Category, error) {
	return o.storage.Category().Create(ctx, request)
}
func (o *categoryService) Get(ctx context.Context, request *pbo.CategoryPrimaryKey) (*pbo.Category, error) {
	return o.storage.Category().Get(ctx, request)
}
func (o *categoryService) GetList(ctx context.Context, request *pbo.GetListRequestCategory) (*pbo.CategoryListResponse, error) {
	return o.storage.Category().GetList(ctx, request)
}
func (o *categoryService) Update(ctx context.Context, request *pbo.Category) (*pbo.Category, error) {
	return o.storage.Category().Update(ctx, request)
}
func (o *categoryService) Delete(ctx context.Context, request *pbo.CategoryPrimaryKey) (*emptypb.Empty, error) {
	return o.storage.Category().Delete(ctx, request)
}
