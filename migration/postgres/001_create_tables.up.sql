CREATE TYPE product_type AS ENUM (
  'modifier',
  'product'
);

CREATE TABLE IF NOT EXISTS categories (
  id uuid PRIMARY KEY,
  title varchar(20),
  active bool DEFAULT true,
  parent_id uuid,
  order_number int,
  created_at timestamp default now(),
  updated_at timestamp,
  deleted_at int default 0
);

CREATE TABLE IF NOT EXISTS products (
  id uuid PRIMARY KEY,
  title varchar(15),
  description text,
  order_number int,
  active bool DEFAULT true,
  type product_type,
  price float,
  category_id uuid,
  created_at timestamp DEFAULT now(),
  updated_at timestamp DEFAULT now(),
  deleted_at int default 0
);

ALTER TABLE categories ADD FOREIGN KEY (parent_id) REFERENCES categories (id);
ALTER TABLE products ADD FOREIGN KEY (category_id) REFERENCES categories (id);

